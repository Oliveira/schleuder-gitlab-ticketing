require 'schleuder-gitlab-ticketing'

class MailStub
  attr_accessor :subject
  def initialize(from, subject, headers={})
    @from = from
    @subject = subject
    @headers = headers
  end

  def message_id
    '1-abc-3'
  end

  def [](a)
    @headers[a]
  end

  def from
    @from_arr ||= Array(@from)
  end

  def original_message
  end
end
