# encoding: utf-8

$: << File.expand_path('../lib', __FILE__)
require 'schleuder-gitlab-ticketing/version'

Gem::Specification.new do |s|
  s.name         = "schleuder-gitlab-ticketing"
  s.version      = SchleuderGitlabTicketing::VERSION
  s.authors      = 'schleuder dev team'
  s.email        = "team@schleuder.org"
  s.homepage     = "https://schleuder.org/"
  s.summary      = "Schleuder Gitlab Ticketing is a filter plugin for schleuder to hook a list into a gitlab issue tracker"
  s.description  = "Schleuder Gitlab Ticketing combines a schleuder list with the issue tracker of a gitlab project and operates as a state tracker of threads on the list. This allows one to keep an overview on the state of various requests on a help desk powered by schleuder"
  s.files        = `git ls-files lib README.md`.split
  s.executables =  %w[]
  s.platform     = Gem::Platform::RUBY
  s.require_path = 'lib'
  s.rubyforge_project = '[none]'
  s.license = 'GPL-3.0'
  s.metadata = {
    "homepage_uri"      => "https://schleuder.org/",
    "documentation_uri" => "https://schleuder.org/docs/",
    "changelog_uri"     => "https://0xacab.org/schleuder/schleuder-gitlab-ticketing/blob/master/CHANGELOG.md",
    "source_code_uri"   => "https://0xacab.org/schleuder/schleuder-gitlab-ticketing/",
    "bug_tracker_uri"   => "https://0xacab.org/schleuder/schleuder-gitlab-ticketing/issues",
    "mailing_list_uri"  => "https://lists.nadir.org/mailman/listinfo/schleuder-announce/",
  }
  s.required_ruby_version = ">= 2.1.0"
  s.add_runtime_dependency 'gitlab', '~> 4.8'
  s.add_runtime_dependency 'schleuder', '~> 3.3'
  s.add_development_dependency 'rspec', '~> 3.5'
  s.post_install_message = "

    To activate the filter plugin you will need to link
    lib/schleuder/filters/post_decryption/99_gitlab_ticketing.rb
    to /usr/local/lib/schleuder/filters/post_decryption/

  "
end
